<?php

namespace App\Entity;

use App\Repository\UserDetailRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as MyAssert;


/**
 * @ORM\Entity(repositoryClass=UserDetailRepository::class)
 */
class UserDetail
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private $userGender;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userAdress;

    /**
     * @ORM\Column(type="integer")
     */
    private $userZipcode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userRegion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userCountry;

    /**
     * @ORM\Column(type="integer")
     */
    private $userMobile;

    /**
     * @ORM\Column(type="integer")
     * MyAssert\PositiveOrZero
     */
    private $userPhone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userPlaceOfBirth;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserGender(): ?string
    {
        return $this->userGender;
    }

    public function setUserGender(string $userGender): self
    {
        $this->userGender = $userGender;

        return $this;
    }

    public function getUserAdress(): ?string
    {
        return $this->userAdress;
    }

    public function setUserAdress(string $userAdress): self
    {
        $this->userAdress = $userAdress;

        return $this;
    }

    public function getUserZipcode(): ?int
    {
        return $this->userZipcode;
    }

    public function setUserZipcode(int $userZipcode): self
    {
        $this->userZipcode = $userZipcode;

        return $this;
    }

    public function getUserRegion(): ?string
    {
        return $this->userRegion;
    }

    public function setUserRegion(string $userRegion): self
    {
        $this->userRegion = $userRegion;

        return $this;
    }

    public function getUserCountry(): ?string
    {
        return $this->userCountry;
    }

    public function setUserCountry(string $userCountry): self
    {
        $this->userCountry = $userCountry;

        return $this;
    }

    public function getUserMobile(): ?int
    {
        return $this->userMobile;
    }

    public function setUserMobile(int $userMobile): self
    {
        $this->userMobile = $userMobile;

        return $this;
    }

    public function getUserPhone(): ?int
    {
        return $this->userPhone;
    }

    public function setUserPhone(int $userPhone): self
    {
        $this->userPhone = $userPhone;

        return $this;
    }

    public function getUserPlaceOfBirth(): ?string
    {
        return $this->userPlaceOfBirth;
    }

    public function setUserPlaceOfBirth(string $userPlaceOfBirth): self
    {
        $this->userPlaceOfBirth = $userPlaceOfBirth;

        return $this;
    }
}
