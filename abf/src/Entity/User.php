<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(
 * fields = {"userEmail"},
 * message = "L'email que vous avez indiqué est déjà utilisé !"
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userLastname;

    /**
     * @ORM\Column(type="date")
     */
    private $userBirthdate;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     */
    private $userEmail;

    /**
     * @Assert\Length(max=4096, min=8, minMessage= " Votre mot de passe doit faire minimun 8 caractères") TO DO
     * @Assert\EqualTo(propertyPath="password")
     */
    private $plainPassword;

    /**
     * The below length depends on the "algorithm" you use for encoding
     * the password, but this works well with bcrypt.
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max=4096, min=8, minMessage= " Votre mot de passe doit faire minimun 8 caractères") TO DO
     * @Assert\EqualTo(propertyPath="plainPassword")
     */
    private $password;

    

    /**
     * @ORM\OneToOne(targetEntity=UserDetail::class, cascade={"persist", "remove"})
     */
    private $user_detail;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUserLastname(): ?string
    {
        return $this->userLastname;
    }

    public function setUserLastname(string $userLastname): self
    {
        $this->userLastname = $userLastname;

        return $this;
    }

    public function getuserBirthdate(): ?\DateTimeInterface
    {
        return $this->userBirthdate;
    }

    public function setuserBirthdate(\DateTimeInterface $userBirthdate): self
    {
        $this->userBirthdate = $userBirthdate;

        return $this;
    }

    public function getuserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setuserEmail(string $userEmail): self
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

  
    public function eraseCredentials (){

    }

    public function getSalt (){

    }

    public function getUserDetail(): ?UserDetail
    {
        return $this->user_detail;
    }

    public function setUserDetail(?UserDetail $user_detail): self
    {
        $this->user_detail = $user_detail;

        return $this;
    }

    public function getRoles(): ?array
    {
        $roles = $this->roles;
        $roles[]= 'ROLE_USER';
    
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
