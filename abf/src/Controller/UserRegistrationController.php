<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRegistrationController extends AbstractController
{
    //     /**
    // * @Route("/user/registration", name="user_registration", methods={"GET"})
    // */
    // public function addForm()
    // {
    //     $user = new User();
    
    //     $form = $this->createForm(UserType::class, $user);
    
    //     return $this->render('user_registration/index.html.twig', [
    //         'action'=>$this->generateUrl('user_registration_post'),
    //         'method'=>'POST'
    //     ]);
    //     return $this->render('user_registration/index.html.twig', [
    //         'controller_name' => 'UserRegistrationController',
    //         'form' => $form->createView(),
    //     ]);
    // }

    /**
     * @Route("/user/registration", name="user_registration")
     */
    public function registerAction(EntityManagerInterface $entityManager,Request $request, userPasswordEncoderInterface $passwordEncoder , MailerInterface $mailer)
    {
        $user = new User();
        
        $form = $this->createForm(UserType::class,$user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form -> get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            
            $email = (new TemplatedEmail())
                ->from('noreply@abf.com')
                ->to($user->getUserEmail())//->getData())
                ->cc('hakim.mohammed@hotmail.fr')
                //->bcc('bcc@example.com')
                //->replyTo('patrick@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject('Confirmation de votre inscription')
                //->text('Sending emails is fun again!')
                //->html('<p>See Twig integration for better HTML integration!</p>');
                // ->text( 'L\'événement  est a valider ! rendez vous sur l\'espace admin');
                ->htmlTemplate('emails/confirm_registration.html.twig');

            $mailer->send($email);

            $this->addFlash('success', 'Votre inscription a bien été enregistrée !');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('user_registration/index.html.twig', [
            'controller_name' => 'UserRegistrationController',
            'form' => $form->createView(),
        ]);
    }

    /**
    * @Route("/user/edit/{id}", name="user_update")
    */
    public function updateUser(User $user,EntityManagerInterface $entityManager, Request $request)
    {
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
        
            $entityManager->flush();
            
            return $this->redirectToRoute('user_registration');
        }

        return $this->render('user_registration/index.html.twig', [
            'controller_name' => 'UserRegistrationController',
            'form' => $form->createView(),
        ]);
    }
    // /**
    //  * @Route("/connexion", name="user_login")
    //  */
    // public function login(){
    //     return $this->render('user_registration/login.html.twig');
    // }

    // /**
    //  * @Route("/deconnexion", name="user_logout")
    //  */
    // public function logout(){
        
    // }
}

