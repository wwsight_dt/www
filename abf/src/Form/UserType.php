<?php

namespace App\Form;

use App\Entity\User;
use App\Form\UserDetailType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\Length;


class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Nom :',
              //  'require' =>true
            ])
            ->add('userlastname', TextType::class, [
                'label' => 'Prénom: ',
                //'require' =>true
            ])
            ->add('userbirthdate', BirthdayType::class, [
                'label' => 'Date de naissance: ',
                'placeholder' => [ 
                    'Day' => "Jour",
                    'Month' => "Mois",
                    'Year'=> "Année",
                ],
                'format' => 'dd-MM-yyyy',
                //'require' =>true
            ])
            ->add('useremail', EmailType::class, [
                'label' => 'Adresse Email: ',
                //'require' => true
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type'            => PasswordType::class,
                'mapped'          => false,  
                'invalid_message' => 'Les mots de passe ne sont pas identique !',
                'first_options'   => ['label' => 'Mot de passe: '],
                'second_options'  => ['label' => 'Confirmer le mot de passe: '],
                "constraints"     =>[
                    new Length([
                    'min' => 8,
                    'max' => 20,
                    'minMessage' => "Votre mot de passe doit contenir au moins 8 caractères",
                    'maxMessage' => "Votre mot de passe doit contenir au plus 20 caractères",
                    ]),]
            ])
            ->add('roles', TextType::class, [
                'label' => 'Role: ',
            ])
            ->addEventListener (FormEvents::PRE_SET_DATA, function (FormEvent $event){
                $user = $event ->getData();
                $form = $event ->getForm();
                if ($user != null && ($user -> getId() == null || $user -> getUserDetail() != null)){
                    $form -> add ('user_detail', UserDetailType::class);
                }
            })

            ->add('accepter', CheckboxType::class, ['mapped' => false])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event){
                $user = $event->getData();
                if (!isset($user['accepter']) || !$user['accepter']){
                    exit;
                }
            })

            // Ceci est un exemple de comment enlever des fields à profile
            ->remove('roles')
            
            ->add('save', SubmitType::class, [
                'label' => 'Inscription !'
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
