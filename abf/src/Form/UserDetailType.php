<?php

namespace App\Form;

use App\Entity\UserDetail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\PositiveOrZero;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class UserDetailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_gender', ChoiceType::class, [
                'label' => 'Sexe: ',
                'choices' => [
                    'Homme' => 'Monsieur',
                    'Femme' => 'Madame',
                ]
            ])
            ->add('user_adress', TextType::class, [
                'label' => 'Adresse:'
            ])
            ->add('user_zipcode', IntegerType::class, [
                'label' => 'Code Postal: '
            ])
            ->add('user_region', TextType::class, [
                'label' => 'Region :'
            ])
            ->add('user_country', CountryType::class, [
                'label' => 'Pays: '
            ])
            ->add('user_mobile', TelType::class, [
                'label' => 'Téléphone portable: ',
            ])
            ->add('user_phone', NumberType::class, [
                'label' => 'Téléphone fixe :'
            ])
            ->add('user_place_of_birth', TextType::class, [
                'label' => 'Lieu de naissance: ',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserDetail::class,
        ]);
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('user_phone', new Assert\PositiveOrZero());
    }
}
